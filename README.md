# Data Studio Authenticated Community Connector

Google Data Studio integration for your Xano authenticated endpoints.

*This is not an official Google product*

This [Community Connector] lets you query data directly in [Data Studio] from any of
your authenticated [Xano] endpoints (If your endpoint does not require authentication check out our
[Regular Connector]).

## Setup

### Direct Installation through Data Studio

You can add this community connector directly in Data Studio with the following steps:

1. Go to [Data Studio].
2. Click **Blank Report**.
3. In the search box within the "Add data to report > connect to data" section search *'Xano Authenticated API*
4. Click on **Xano Authenticated  API** to add it.
5. You will prompted to authenticate, add your Xano authentication endpoint that you've set up for the endpoint you want 
   to import data from and enter your email and password.
6. Finally, add your Xano Data Endpoint (This will be your [Xano] base url followed by the endpoint
   you want to import into Data Studio).
7. If your endpoint does not require authentication see our [Regular Connector].

### Custom Private Direct Installation

If there is something you'd like to see integrate in future releases please [contact us], otherwise if
you would like something for your specific use-case you can directly install this Community Connector via
[Google Scripts] (Note: you do not have permission to publish a community connector using the Xano brand,
any modifications are for personal use only).

1. Clone this repository.
2. Go to [Google Scripts].
3. Click on **+ New project** to create a new project.
4. Paste the code.js file contents into Code.gs.
5. Modify as you'd like.
6. If you need to access the Manifest file go to project settings on the left menu, then under
   project settings click **Show "appsscript.json" manifest file in editor** then go back to the code-editor
   and modify as needed.
6. When you're ready to test out click **Deploy** > ***New Deploy***
7. In the modal popup **select type** of deployment fill out any configuration details then click
   **Deploy**
8. Copy your **Deployment ID** then head over to [Data Studio] create a new project and in the community
   connects search *Build your Own* then add paste in your **Deployment ID**.

## Support

The Xano team is actively supporting this community connector. If you have some features you would like
us to implement, or you run into any issues and need help troubleshooting, would like to report a bug,
or need help getting setup please fill out the form on our [Community Connector Support Page].

Happy Coding,

–The Xano Team


[Google Scripts]: https://script.google.com/home
[Xano]: https://www.xano.com
[contact us]: http://xano.com/connect/google-data-studio
[Community Connector Support Page]: http://xano.com/connect/google-data-studio
[Data Studio]: https://datastudio.google.com
[Community Connector]: https://developers.google.com/datastudio/connector
[Regular Connector]: https://gitlab.com/xano-integrations/dscc
