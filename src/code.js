const cc = DataStudioApp.createCommunityConnector();

sendUserError = (message) => {
	cc.newUserError()
		.setText(message)
		.throwException();
}

getConfig = (request) => {
	const config = cc.getConfig();

	config
		.newInfo()
		.setId('instructions')
		.setText('Connect to your Authenticated Xano API');

	config
		.newTextInput()
		.setId('url')
		.setName('Enter your Xano Data Endpoint')
		.setHelpText('e.g. https://xxxx-xxxx-xxxx.n7.xano.io/api:xxxxxxxx/endpoint')
		.setPlaceholder('https://xxxx-xxxx-xxxx.n7.xano.io/api:xxxxxxxx/endpoint');

	config.setDateRangeRequired(false);

	return config.build();
}

fetchJSON = (url) => {
	try {
		let pageURL = url;
		let data = [];
		let fetchNext = true;
		do {

			//auth
			const userProperties = PropertiesService.getUserProperties();
			const token = userProperties.getProperty('token');
			const response = UrlFetchApp.fetch(pageURL, {"headers": {"authorization": "Bearer " + token}});
			const parsedResponse = JSON.parse(response);

			if (!parsedResponse.hasOwnProperty('items')) {
				data = parsedResponse;
				fetchNext = false;
				break;
			} else {
				data = [...data, ...parsedResponse.items];
				if (parsedResponse.nextPage) {
					pageURL = url + '?search=' + encodeURI(JSON.stringify({"page": parsedResponse.nextPage}))
				} else {
					fetchNext = false;
				}
			}
		} while (fetchNext);
		return data;
	} catch (e) {
		sendUserError(e);
	}
}

fetchData = (url) => {
	if (!url || !url.match(/^https?:\/\/.+$/g)) {
		sendUserError('"' + url + '" is not a valid url.');
	}
	const data = fetchJSON(url);
	if (!data) sendUserError('"' + url + '" returned no data.');

	return data;
}

getType = (value, types) => {
	if (!isNaN(parseFloat(value)) && isFinite(value)) {
		return types.NUMBER;
	} else if (value === true || value === false) {
		return types.BOOLEAN;
	} else if (typeof value != 'object' && value != null) {
		if (
			value.match(
				new RegExp(
					/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi
				)
			)
		) {
			return types.URL;
		} else if (!isNaN(Date.parse(value))) {
			return types.YEAR_MONTH_DAY_HOUR;
		}
	}
	return types.TEXT;
}

createField = (fields, types, key, value) => {
	const type = getType(value, types);
	const field = type === types.NUMBER ? fields.newMetric() : fields.newDimension();

	field.setType(type);
	field.setId(key.replace(/\s/g, '_').toLowerCase());
	field.setName(key);
}

getElementKey = (key, currentKey) => {
	if (currentKey === '' || currentKey === null) {
		return;
	}
	if (key !== null) {
		return key + '.' + currentKey.replace('.', '_');
	}
	return currentKey.replace('.', '_');
}

createFields = (fields, types, key, value) => {
	if (typeof value === 'object' && !Array.isArray(value) && value !== null) {
		Object.keys(value).forEach(function(currentKey) {
			const elementKey = getElementKey(key, currentKey);
			value[currentKey] !== null ?  createFields(fields, types, elementKey, value[currentKey]): createField(fields, types, currentKey, value);
		});
	} else if (key !== null) {
		createField(fields, types, key, value);
	}
}

getFields = (request, data) => {
	const fields = cc.getFields();
	const types = cc.FieldType;
	const aggregations = cc.AggregationType;

	if (!Array.isArray(data)) data = [data];

	if (typeof data[0] !== 'object' || data[0] === null) {
		sendUserError('Invalid JSON');
	}
	try {
		createFields(fields, types, null, data[0]);
	} catch (e) {
		sendUserError('Unable to identify the data format of one of your fields.');
	}
	return fields;
}

getSchema = (request) => {
	const data = fetchData(request.configParams.url);
	const fields = getFields(request, data).build();
	return {schema: fields};
}

mergeDeep = () => {
	const objects = Array.prototype.slice.call(arguments);

	return objects.reduce(function(prev, obj) {
		Object.keys(obj).forEach(function(key) {
			const pVal = prev[key];
			const oVal = obj[key];

			if (Array.isArray(pVal) && Array.isArray(oVal)) {
				prev[key] = pVal.concat.apply(pVal, toConsumableArray(oVal));
			} else if (pVal === Object(pVal) && oVal === Object(oVal)) {
				prev[key] = mergeDeep(pVal, oVal);
			} else {
				prev[key] = oVal;
			}
		});
		return prev;
	}, {});
}

convertDate = (val) => {
	const date = new Date(val);
	return (
		date.getUTCFullYear() +
		('0' + (date.getUTCMonth() + 1)).slice(-2) +
		('0' + date.getUTCDate()).slice(-2) +
		('0' + date.getUTCHours()).slice(-2)
	);
}

validateValue = (field, val) => {
	if (field.getType() == 'YEAR_MONTH_DAY_HOUR') {
		val = convertDate(val);
	}

	switch (typeof val) {
		case 'string':
		case 'number':
		case 'boolean':
			return val;
		case 'object':
			return JSON.stringify(val);
		default: return '';
	}
}

getColumnValue = (valuePaths, row) => {
	for (const index in valuePaths) {
		const currentPath = valuePaths[index];

		if (row[currentPath] === null) {
			return '';
		}

		if (row[currentPath] !== undefined) {
			row = row[currentPath];
			continue;
		}
		const keys = Object.keys(row);

		for (const index_keys in keys) {
			const key = keys[index_keys].replace(/\s/g, '_').toLowerCase();
			if (key === currentPath) {
				row = row[keys[index_keys]];
				break;
			}
		}
	}
	return row;
}

getColumns = (data, requestedFields) => {
	if (!Array.isArray(data)) data = [data];

	return data.map(function(row) {
		const rowValues = [];

		requestedFields.asArray().forEach(function(field) {
			const valuePaths = field.getId().split('.');
			const fieldValue = row === null ? '' : getColumnValue(valuePaths, row);

			rowValues.push(validateValue(field, fieldValue));
		});
		return {values: rowValues};
	});
}

getData = (request) => {
	const data = fetchData(request.configParams.url);
	const fields = getFields(request, data);
	const requestedFieldIds = request.fields.map(field => field.name);
	const requestedFields = fields.forIds(requestedFieldIds);

	return {
		schema: requestedFields.build(),
		rows: getColumns(data, requestedFields)
	};
}

getAuthType = () => {
	return cc.newAuthTypeResponse()
		.setAuthType(cc.AuthType.PATH_USER_PASS)
		.setHelpUrl('https://www.example.org/connector-auth-help')
		.build();
}

isAuthValid = () => {
	const userProperties = PropertiesService.getUserProperties();
	const path = userProperties.getProperty('dscc.path');
	const userName = userProperties.getProperty('dscc.username');
	const password = userProperties.getProperty('dscc.password');
	return validateCredentials(path, userName, password);
}

resetAuth = () => {
	const userProperties = PropertiesService.getUserProperties();
	userProperties.deleteProperty('dscc.path');
	userProperties.deleteProperty('dscc.username');
	userProperties.deleteProperty('dscc.password');
}

validateCredentials = (path, username, password) => {
	if(path) {
		try {
			const rawResponse = UrlFetchApp.fetch(path, {
				method: 'POST',
				payload: {
					email: username,
					password: password
				}
			});
			if (rawResponse) {
				const userProperties = PropertiesService.getUserProperties()
				const parsedResponse = JSON.parse(rawResponse);
				userProperties.setProperty('token', parsedResponse.authToken)
				return parsedResponse.hasOwnProperty('authToken') && parsedResponse.authToken.length > 0 ?  true : false;
			}
		} catch(e) {
			return false
		}
	}
	return false
}

setCredentials = (request) => {
	const creds = request.pathUserPass;
	const path = creds.path;
	const username = creds.username;
	const password = creds.password;
	const userProperties = PropertiesService.getUserProperties();
	userProperties.setProperty('dscc.path', path);
	userProperties.setProperty('dscc.username', username);
	userProperties.setProperty('dscc.password', password);
	return {
		errorCode: 'NONE'
	};
}

isAdminUser = () => {
	return false;
}
